package com.family.familytest.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.family.familytest.R;
import com.family.familytest.adapters.MembersAdapter;
import com.family.familytest.bdd.daos.FamilyDAO;
import com.family.familytest.bdd.interfaces.GetChildsBDDInterface;
import com.family.familytest.customviews.SimpleDividerItemDecoration;
import com.family.familytest.models.Member;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListaHijosActivity extends AppCompatActivity implements GetChildsBDDInterface {

    //region bind
    @Bind(R.id.list_members)
    RecyclerView listMembers;
    private MembersAdapter mAdapter;
    //endregion

    //region lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_hijos);
        ButterKnife.bind(this);
        initToolbar();
        new FamilyDAO().getChilds(this, this);
    }

    private void initToolbar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.child_list_screen));
    }

    private void initMembersLayout(ArrayList<Member> memberRealms) {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        listMembers.setLayoutManager(llm);
        listMembers.addItemDecoration(new SimpleDividerItemDecoration(this, SimpleDividerItemDecoration.VERTICAL_LIST));
        mAdapter = new MembersAdapter(this, memberRealms);
        listMembers.setAdapter(mAdapter);
    }
    //endregion

    //region data
    @Override
    public void getChildsData(ArrayList<Member> memberRealms) {
        initMembersLayout(memberRealms);
    }
    //endregion

    //region menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }
    //endregion
}
