package com.family.familytest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.family.familytest.R;
import com.family.familytest.bdd.daos.FamilyDAO;
import com.family.familytest.bdd.interfaces.ChildSavedListenerInterface;
import com.family.familytest.bdd.models.MemberRealm;
import com.family.familytest.models.Member;
import com.family.familytest.utils.MyPreferences;
import com.family.familytest.utils.StringConstants;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HijoActivity extends AppCompatActivity implements ChildSavedListenerInterface{

    @Bind(R.id.et_name)
    EditText etName;
    @Bind(R.id.et_surname)
    EditText etSurname;
    @Bind(R.id.et_age)
    EditText etAge;
    @Bind(R.id.et_sex)
    EditText etSex;
    private MyPreferences myPreferences;

    //region lifeCycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hijo);
        ButterKnife.bind(this);
        myPreferences=new MyPreferences(this);
        initToolbar();
    }

    private void initToolbar() {
        setTitle(getString(R.string.hijo_screen));
    }
    //endregion

    //region click
    @OnClick(R.id.tv_save_son)
    public void onViewClicked() {
        if(!(TextUtils.isEmpty(etName.getText().toString())) &&
                !(TextUtils.isEmpty(etSurname.getText().toString()))&&
                        !(TextUtils.isEmpty(etAge.getText().toString()))&&
                                (!TextUtils.isEmpty(etSex.getText().toString()))){
            new FamilyDAO().saveChild(new MemberRealm(myPreferences.getIntPreference(StringConstants.NEXT_MEMBER_ID),
                    getRol(etSex.getText().toString()),
                    Integer.valueOf(etAge.getText().toString()),
                    etName.getText().toString(),
                    etSurname.getText().toString(),
                    etSex.getText().toString()),this,this);
        }
    }
    //endregion

    //region extras
    private int getRol(String sex) {
        if (sex.equalsIgnoreCase("M")) {
            return Member.HIJO;
        }else{
            return Member.HIJA;
        }

    }

    @Override
    public void childSaved(Member memberRealms) {
        Toast.makeText(this, getString(R.string.child_saved), Toast.LENGTH_SHORT).show();
        Intent result = new Intent();
        result.putExtra("member",memberRealms);
        setResult(RESULT_OK, result);
        finish();
    }
    //endregion
}
