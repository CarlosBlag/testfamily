package com.family.familytest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.family.familytest.R;
import com.family.familytest.adapters.MembersAdapter;
import com.family.familytest.bdd.daos.FamilyDAO;
import com.family.familytest.bdd.interfaces.GetFamilyBDDInterface;
import com.family.familytest.customviews.SimpleDividerItemDecoration;
import com.family.familytest.models.Member;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity implements GetFamilyBDDInterface {

    //region bind
    private static final int ADD_CHILD = 111;
    @Bind(R.id.list_members)
    RecyclerView listMembers;
    private MembersAdapter mAdapter;
    private ArrayList<Member> membersArray;
    //endregion

    //region lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        initToolbar();
        membersArray = (ArrayList<Member>) getIntent().getSerializableExtra("members");
        initMembersLayout();

    }

    private void initToolbar() {
        setTitle(getString(R.string.home_screen));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_CHILD) {
            if (resultCode == RESULT_OK) {
                getMemberFromRealm();
            }
        }
    }

    private void initMembersLayout() {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        listMembers.setLayoutManager(llm);
        listMembers.addItemDecoration(new SimpleDividerItemDecoration(this, SimpleDividerItemDecoration.VERTICAL_LIST));
        mAdapter = new MembersAdapter(this, membersArray);
        listMembers.setAdapter(mAdapter);
    }

    //endregion

    //region click
    @OnClick(R.id.tv_add_son)
    public void onViewClicked() {
        startActivityForResult(new Intent(this, HijoActivity.class), ADD_CHILD);
    }
    //endregion

    //region data
    private void getMemberFromRealm() {
        new FamilyDAO().getFamily(this, this);
    }

    @Override
    public void getFamilyData(ArrayList<Member> memberRealms) {
        mAdapter.clear();
        mAdapter.addItems(memberRealms);
    }
    //endregion

    //region menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_see_childs:
                startActivity(new Intent(this, ListaHijosActivity.class));
                return true;
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }
    //endregion
}
