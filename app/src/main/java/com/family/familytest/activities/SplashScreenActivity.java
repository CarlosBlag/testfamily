package com.family.familytest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.family.familytest.R;
import com.family.familytest.bdd.daos.FamilyDAO;
import com.family.familytest.bdd.interfaces.FamilySavedListenerInterface;
import com.family.familytest.bdd.interfaces.GetFamilyBDDInterface;
import com.family.familytest.bdd.models.MemberRealm;
import com.family.familytest.models.Member;
import com.family.familytest.utils.MyPreferences;
import com.family.familytest.utils.StringConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SplashScreenActivity extends AppCompatActivity implements FamilySavedListenerInterface,
    GetFamilyBDDInterface{

    @Bind(R.id.textView)
    TextView textView;
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    private MyPreferences myPreferences;
    private ArrayList<Member> members;

    //region lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        myPreferences = new MyPreferences(this);
        initToolbar();
        if (myPreferences.getBooleanPreference(StringConstants.FAMILY_SAVED)) {
           getMemberFromRealm();
        } else {
            new FamilyDAO().saveFamily(this, getMiembrosFromJson(), this);
        }
    }

    private void initToolbar() {
        setTitle(getString(R.string.splash_screen));
    }
    //endregion

    //region json
    private ArrayList<MemberRealm> getMiembrosFromJson() {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<MemberRealm>>() {
        }.getType();
        ArrayList<MemberRealm> memberRealms = gson.fromJson(getString(R.string.jsonFamily), listType);
        return memberRealms;
    }
    //endregion

    //region data
    private void getMemberFromRealm() {
        new FamilyDAO().getFamily(this,this);
    }
    //endregion
    //region callback
    @Override
    public void familySaved(ArrayList<Member> members) {
        myPreferences.savePreference(StringConstants.FAMILY_SAVED, true);
        progressBar.setVisibility(View.GONE);
        this.members=members;
        goHome();
    }

    @Override
    public void getFamilyData(ArrayList<Member> members) {
        progressBar.setVisibility(View.GONE);
        this.members=members;
        goHome();
    }
    //endregion

    //region extras
    private void goHome() {
        Intent intent = new Intent(this,HomeActivity.class);
        intent.putExtra("members",members);
        startActivity(intent);
        finish();
    }
    //endregion
}
