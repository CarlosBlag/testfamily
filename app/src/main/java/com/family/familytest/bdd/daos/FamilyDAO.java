package com.family.familytest.bdd.daos;

import android.content.Context;

import com.family.familytest.base_bdd.RealmManager;
import com.family.familytest.bdd.interfaces.ChildSavedListenerInterface;
import com.family.familytest.bdd.interfaces.FamilySavedListenerInterface;
import com.family.familytest.bdd.interfaces.GetChildsBDDInterface;
import com.family.familytest.bdd.interfaces.GetFamilyBDDInterface;
import com.family.familytest.bdd.models.MemberRealm;
import com.family.familytest.bdd.tasks.SaveChildRealmBDDTask;
import com.family.familytest.bdd.tasks.SaveFamilyRealmBDDTask;
import com.family.familytest.mappers.MemberRealmMapper;
import com.family.familytest.models.Member;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by cbla on 18/4/16.
 */
public class FamilyDAO {

    private ArrayList<Member> members;
    private MemberRealmMapper mapper;

    //region save
    public void saveFamily(Context context, ArrayList<MemberRealm> memberRealms, FamilySavedListenerInterface savedListener) {
        new SaveFamilyRealmBDDTask(context, memberRealms, savedListener).execute();
    }

    public void saveChild(MemberRealm memberRealm, Context context, ChildSavedListenerInterface childSavedListenerInterface) {
        new SaveChildRealmBDDTask(context,memberRealm,childSavedListenerInterface).execute();
    }
    //endregion

    //region gets
    public void getFamily(Context context, GetFamilyBDDInterface getFamilyBDDInterface) {
        Realm realm = null;
        members=new ArrayList<>();
        mapper=new MemberRealmMapper();
        try {
            realm = new RealmManager().getRealInstance();
            RealmResults<MemberRealm> membersRealm=realm.where(MemberRealm.class).findAllSorted("age", Sort.DESCENDING);
            if(membersRealm!=null){
                for(MemberRealm memberRealm:membersRealm){
                    members.add(mapper.map(memberRealm));
                }
                getFamilyBDDInterface.getFamilyData(members);
            }
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public void getChilds(Context context, GetChildsBDDInterface getChildsBDDInterface) {
        Realm realm = null;
        members=new ArrayList<>();
        mapper=new MemberRealmMapper();
        try {
            realm = new RealmManager().getRealInstance();
            RealmResults<MemberRealm> membersRealm=realm.where(MemberRealm.class)
                    .beginGroup()
                        .equalTo("rol",3)
                        .or()
                        .equalTo("rol",4)
                    .endGroup()
                    .findAllSorted("age", Sort.DESCENDING);
            if(membersRealm!=null){
                for(MemberRealm memberRealm:membersRealm){
                    members.add(mapper.map(memberRealm));
                }
                getChildsBDDInterface.getChildsData(members);
            }
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    //endregion
}
