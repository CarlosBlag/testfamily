package com.family.familytest.bdd.interfaces;


import com.family.familytest.models.Member;

import java.util.ArrayList;

/**
 * Created by cbla on 12/4/16.
 */
public interface GetChildsBDDInterface {
       void getChildsData(ArrayList<Member> memberRealms);
}
