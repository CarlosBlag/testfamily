package com.family.familytest.bdd.models;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Duca on 04/07/2017.
 */

public class MemberRealm extends RealmObject implements Serializable {
    protected static final long serialVersionUID = -1147224215165824085L;

    @PrimaryKey
    int id;
    int rol,age;
    String name, surname,sex;

    public MemberRealm() {
    }

    public MemberRealm(int id, int rol, int age, String name, String surname, String sex) {
        this.id = id;
        this.rol = rol;
        this.age = age;
        this.name = name;
        this.surname = surname;
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
