package com.family.familytest.bdd.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.family.familytest.base_bdd.RealmManager;
import com.family.familytest.bdd.interfaces.ChildSavedListenerInterface;
import com.family.familytest.bdd.models.MemberRealm;
import com.family.familytest.mappers.MemberRealmMapper;
import com.family.familytest.models.Member;
import com.family.familytest.utils.MyPreferences;

import io.realm.Realm;

public class SaveChildRealmBDDTask extends AsyncTask<Void, Void, Void> {
    private final Context context;
    private final RealmManager realmManager;
    private final MemberRealm memberRealm;
    private final ChildSavedListenerInterface savedListener;
    private final MemberRealmMapper memberMapper;
    private final MyPreferences myPreferences;
    private Realm realm;
    private Member member;

    public SaveChildRealmBDDTask(Context context, MemberRealm memberRealm, ChildSavedListenerInterface savedListener) {
        this.context = context;
        this.memberRealm = memberRealm;
        realmManager = new RealmManager();
        memberMapper = new MemberRealmMapper();
        this.savedListener = savedListener;
        myPreferences = new MyPreferences(context);
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            realm = realmManager.getRealInstance();

            realm.beginTransaction();
            realm.copyToRealmOrUpdate(memberRealm);
            realm.commitTransaction();

            member=memberMapper.map(memberRealm);

        } catch (Exception e) {
            realm.cancelTransaction();
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        savedListener.childSaved(member);
    }
}

