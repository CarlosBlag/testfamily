package com.family.familytest.bdd.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.family.familytest.base_bdd.RealmManager;
import com.family.familytest.bdd.interfaces.FamilySavedListenerInterface;
import com.family.familytest.bdd.models.MemberRealm;
import com.family.familytest.mappers.MemberRealmMapper;
import com.family.familytest.models.Member;
import com.family.familytest.utils.MyPreferences;
import com.family.familytest.utils.StringConstants;

import java.util.ArrayList;

import io.realm.Realm;

public class SaveFamilyRealmBDDTask extends AsyncTask<Void, Void, Void> {
    private final Context context;
    private final RealmManager realmManager;
    private final ArrayList<MemberRealm> memberRealms;
    private final FamilySavedListenerInterface savedListener;
    private final MemberRealmMapper memberMapper;
    private final MyPreferences myPreferences;
    private Realm realm;
    private ArrayList<Member> members;

    public SaveFamilyRealmBDDTask(Context context, ArrayList<MemberRealm> memberRealms, FamilySavedListenerInterface savedListener) {
        this.context = context;
        this.memberRealms = memberRealms;
        realmManager = new RealmManager();
        memberMapper=new MemberRealmMapper();
        this.savedListener = savedListener;
        myPreferences = new MyPreferences(context);
    }

    @Override
    protected Void doInBackground(Void... params) {
        members=new ArrayList<Member>();
        try {
            realm = realmManager.getRealInstance();
            for(MemberRealm memberRealm : memberRealms){
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(memberRealm);
                realm.commitTransaction();
               members.add(memberMapper.map(memberRealm));
            }
            myPreferences.savePreference(StringConstants.NEXT_MEMBER_ID,members.size());
        } catch (Exception e) {
            realm.cancelTransaction();
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        savedListener.familySaved(members);
    }
}

