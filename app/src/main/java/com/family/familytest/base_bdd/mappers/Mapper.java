package com.family.familytest.base_bdd.mappers;

public interface Mapper<M, P> {
    P map(M model);
}

