package com.family.familytest.base_bdd;

import android.content.Context;
import android.os.AsyncTask;

import io.realm.Realm;

public class DeleteAllRealmBDDTask extends AsyncTask<Void, Void, Void> {
    private final Context context;
    private final RealmManager realmManager;
    private Realm realm;

    public DeleteAllRealmBDDTask(Context context) {
        this.context = context;
        realmManager = new RealmManager();

    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            realm = realmManager.getRealInstance();
            realm.beginTransaction();
            realm.deleteAll();
            realm.commitTransaction();
        } catch (Exception e) {
            realm.cancelTransaction();
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();

            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

    }
}

