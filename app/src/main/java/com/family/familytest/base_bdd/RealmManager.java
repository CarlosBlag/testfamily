package com.family.familytest.base_bdd;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by cbla on 24/11/15.
 */
public class RealmManager {


    public Realm getRealInstance() {

        return Realm.getInstance(getMyConfig());

    }

    public RealmConfiguration getMyConfig() {

        return new RealmConfiguration.Builder()
                .name("Family")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
    }
}
