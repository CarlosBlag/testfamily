package com.family.familytest.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class MyPreferences {


    private static final String NAME_PREFERENCES = "myPreferences";
    private final Context context;
    SharedPreferences settings;

    public MyPreferences(Context context) {
        this.settings = context.getSharedPreferences(NAME_PREFERENCES, Context.MODE_PRIVATE);
        this.context = context;
    }



    public void removePreference(String nomVariable) {
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(nomVariable);
        editor.commit();
    }

    public void savePreference(String nomVariable, String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(nomVariable, value);
        editor.commit();
    }

    public void savePreference(String nomVariable, Boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(nomVariable, value);
        editor.commit();
    }

    public void savePreference(String nomVariable, int value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(nomVariable, value);
        editor.commit();
    }

    public void savePreference(String nomVariable, long value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(nomVariable, value);
        editor.commit();
    }


    public boolean contains(String nomVariable) {
        return settings.contains(nomVariable);
    }

    public int getIntPreference(String nomVariable) {
        return settings.getInt(nomVariable, 0);
    }

    public String getStringPreference(String nomVariable) {
        return settings.getString(nomVariable, "");
    }

    public long getLongPreference(String nomVariable) {
        return settings.getLong(nomVariable, 0l);
    }


    public boolean getBooleanPreference(String nomVariable) {
        return settings.getBoolean(nomVariable, false);
    }

    public boolean getBooleanPreference(String nomVariable, boolean defaultValue) {
        return settings.getBoolean(nomVariable, defaultValue);
    }

    public void removeAll() {
        settings.edit().clear().apply();
    }
}
