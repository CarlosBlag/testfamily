package com.family.familytest.utils;

/**
 * Created by Duca on 04/07/2017.
 */

public class StringConstants {
    public static final String FAMILY_SAVED = "family saved";
    public static final String NEXT_MEMBER_ID = "next member id";
}
