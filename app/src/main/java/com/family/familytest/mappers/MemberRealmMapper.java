package com.family.familytest.mappers;

import com.family.familytest.base_bdd.mappers.Mapper;
import com.family.familytest.bdd.models.MemberRealm;
import com.family.familytest.models.Member;

/**
 * Created by Duca on 05/07/2017.
 */

public class MemberRealmMapper implements Mapper<MemberRealm, Member> {

    @Override
    public Member map(MemberRealm model) {
        if (model == null) {
            return null;
        }

        Member member = new Member();
        member.setId(model.getId());
        member.setAge(model.getAge());
        member.setName(model.getName());
        member.setRol(model.getRol());
        member.setSex(model.getSex());
        member.setSurname(model.getSurname());
        return member;
    }
}
