package com.family.familytest.models;

import java.io.Serializable;

/**
 * Created by Duca on 04/07/2017.
 */

public class Member implements Serializable {
    protected static final long serialVersionUID = -1147224215165824085L;
    public static final int PADRE=1;
    public static final int MADRE=2;
    public static final int HIJO=3;
    public static final int HIJA=4;
    public static final int MASCOTA=5;

    int id;
    int rol,age;
    String name, surname,sex;

    public Member() {
    }

    public Member(int id, int rol, int age, String name, String surname, String sex) {
        this.id = id;
        this.rol = rol;
        this.age = age;
        this.name = name;
        this.surname = surname;
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
