package com.family.familytest.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.family.familytest.R;
import com.family.familytest.models.Member;


/**
 * Created by Duca on 04/06/2017.
 */

public class MembersViewHolder extends RecyclerView.ViewHolder {


    private final Context mContext;


    private TextView tvText;

    public MembersViewHolder(View itemView, Context context) {
        super(itemView);
        tvText = (TextView) itemView.findViewById(R.id.tv_item_text);
        mContext = context;
    }

    public void bind(final Member member) {
        tvText.setText(String.format(
                mContext.getString(R.string.item_text),
                member.getName(),
                member.getSurname(),
                String.valueOf(member.getAge()),
                member.getSex(),
                getRol(member.getRol())));
    }

    private String getRol(int rol) {
        String rolString = "";
        switch (rol) {
            case Member.PADRE:
                rolString = "Padre";
                break;
            case Member.MADRE:
                rolString = "Madre";
                break;
            case Member.HIJO:
                rolString = "Hijo";
                break;
            case Member.HIJA:
                rolString = "Hija";
                break;
            case Member.MASCOTA:
                rolString = "Mascota";
                break;
        }
        return rolString;
    }
}
