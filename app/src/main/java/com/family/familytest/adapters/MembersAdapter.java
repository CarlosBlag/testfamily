package com.family.familytest.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.family.familytest.R;
import com.family.familytest.models.Member;
import com.family.familytest.viewholders.MembersViewHolder;

import java.util.List;

/**
 * Created by Duca on 04/06/2017.
 */

public class MembersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final LayoutInflater mInflator;
    private final Context mContext;
    private final List<Member> members;


    public MembersAdapter(Context context, List<Member> members) {
        mContext = context;
        mInflator = LayoutInflater.from(context);
        this.members = members;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MembersViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_member, parent, false),
                 mContext);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MembersViewHolder) holder).bind(members.get(position));
    }

    @Override
    public int getItemCount() {
        return members.size();
    }

    public void addItems(List<Member> items) {
        members.addAll(items);
        notifyDataSetChanged();
    }

    public void clear() {
        members.clear();
        notifyDataSetChanged();
    }
}

